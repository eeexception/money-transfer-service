# money-transfer-service

Example of RESTful API for transferring money developed using Spark java (http://sparkjava.com/) and Gson (https://github.com/google/gson) libraries.
    
It uses Spark as web framework and as in-memory DB it's used ConcurrentHashMap class.
    
To build the app use this maven command:  
`mvn clean package -DskipTests=true`  
The result `money-transfer-service-1.1-SNAPSHOT-jar-with-dependencies.jar` jar file can be found after this at the `releases` directory.  
    
To start up the app from the command line use this command:  
`java -jar money-transfer-service-1.1-SNAPSHOT-jar-with-dependencies.jar`  

It's possible to initialize database using as argument json string with assets. For example  
`java -jar money-transfer-service-1.1-SNAPSHOT-jar-with-dependencies.jar "[{\"accountId\":\"1\", \"amount\":\"100\"}, {\"accountId\":\"2\", \"amount\":\"100\"}, {\"accountId\":\"3\", \"amount\":\"100\"},{\"accountId\":\"4\", \"amount\":\"100\"}]"`
By default server starts at port `8080`.

    

For running tests look at the `org.eeexception.moneytransfer.MoneyTransferAppTest` class.

    

API documentation:  
  All requests return such json response:   
        `{"status":"SUCCESS", "message":"", "data":"true"}`  
        where `status` - SUCCESS if the request is successful and ERROR if there was error.  
        `message` - error message if there was error.  
        `data` - operation result object.    
  * POST /transfers - execute transfer from one account to another  
    Example: `curl --header "Content-Type: application/json" --request POST --data '{"fromAccountId":1, "toAccountId":2, "amount":10' http://localhost:8080/transfers`  
    `data` field of the response contains true if the operation was successful.
  * GET /transfers - receive list of all transfers  
    Example: `curl --header "Content-Type: application/json" --request GET http://localhost:8080/transfers`  
  * POST /assets - put or update account asset by account id  
    Example: `curl --header "Content-Type: application/json" --request POST --data '{"accountId":5,"amount":100}' http://localhost:8080/assets`  
    `data` field of the response contains true if the operation was successful.
  * GET /assets - receive list of all account assets  
    Example: `curl --header "Content-Type: application/json" --request GET http://localhost:8080/assets`  