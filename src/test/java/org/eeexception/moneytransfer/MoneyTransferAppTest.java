package org.eeexception.moneytransfer;

import com.despegar.http.client.GetMethod;
import com.despegar.http.client.HttpResponse;
import com.despegar.http.client.PostMethod;
import com.despegar.sparkjava.test.SparkServer;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.eeexception.moneytransfer.controllers.StandardResponse;
import org.eeexception.moneytransfer.controllers.StatusResponse;
import org.eeexception.moneytransfer.db.DefaultRepositoryManager;
import org.eeexception.moneytransfer.db.RepositoryManager;
import org.eeexception.moneytransfer.entities.AccountAsset;
import org.eeexception.moneytransfer.entities.Transfer;
import org.eeexception.moneytransfer.utils.JsonUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import spark.servlet.SparkApplication;

import static org.eeexception.moneytransfer.MoneyTransferApp.initRoutes;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class MoneyTransferAppTest {
    private static final RepositoryManager repositoryManager = new DefaultRepositoryManager();

    public static class TestMoneyTransferServer implements SparkApplication {
        @Override
        public void init() {
            initRoutes(repositoryManager);
        }
    }

    private class ResponseData<T> {
        private T data;
        private StatusResponse status;

        public ResponseData(T data, StatusResponse status) {
            this.data = data;
            this.status = status;
        }

        public T getData() {
            return data;
        }

        public StatusResponse getStatus() {
            return status;
        }

        @Override
        public String toString() {
            return "ResponseData{" +
                "data=" + data +
                ", status=" + status +
                '}';
        }
    }

    @ClassRule
    public static SparkServer<TestMoneyTransferServer> testServer = new SparkServer<>(TestMoneyTransferServer.class, 18080);

    @Before
    public void initData() {
        System.out.println("Init test data.");

        repositoryManager.updateAsset(new AccountAsset(1L, BigDecimal.valueOf(100)));
        repositoryManager.updateAsset(new AccountAsset(2L, BigDecimal.valueOf(100)));
        repositoryManager.updateAsset(new AccountAsset(3L, BigDecimal.valueOf(100)));
        repositoryManager.updateAsset(new AccountAsset(4L, BigDecimal.valueOf(100)));
    }

    @After
    public void cleanData() {
        System.out.println("Clean data.");

        repositoryManager.deleteAllTransfers();
        repositoryManager.deleteAllAssets();
    }

    @Test
    public void testTransfersController() throws Exception {
        repositoryManager.insertTransfer(new Transfer(1L, 2L, BigDecimal.valueOf(1)));
        repositoryManager.insertTransfer(new Transfer(2L, 3L, BigDecimal.valueOf(2)));
        repositoryManager.insertTransfer(new Transfer(3L, 4L, BigDecimal.valueOf(3)));

        List<Transfer> transfers = executeGet(Routes.TRANSFERS_ROUTE, new TypeToken<StandardResponse<List<Transfer>>>() {}.getType());

        assertNotNull(transfers);
        assertEquals(3, transfers.size());

        for(Transfer transfer : transfers) {
            assertEquals(false, transfer.isExecuted());
        }
    }

    @Test
    public void testTransferController() throws Exception {
        ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
            new Transfer(1L, 20L, BigDecimal.valueOf(10)),
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        assertEquals(StatusResponse.SUCCESS, result.getStatus());
        assertTrue(result.getData());

        List<Transfer> transfers = executeGet(Routes.TRANSFERS_ROUTE, new TypeToken<StandardResponse<List<Transfer>>>() {}.getType());

        for(Transfer transfer : transfers) {
            System.out.println(transfer);
        }

        assertNotNull(transfers);

        assertEquals(1, transfers.size());

        assertTrue(transfers.get(0).isExecuted());
        assertEquals(Long.valueOf(1), transfers.get(0).getFromAccountId());
        assertEquals(Long.valueOf(20), transfers.get(0).getToAccountId());

        AccountAsset accountAsset1 = repositoryManager.getAccountAsset(1L);
        AccountAsset accountAsset20 = repositoryManager.getAccountAsset(20L);

        assertTrue(BigDecimal.valueOf(90).compareTo(accountAsset1.getAmount()) == 0);
        assertTrue(BigDecimal.valueOf(10).compareTo(accountAsset20.getAmount()) == 0);
    }

    @Test
    public void testTransferControllerWithOverSize1() throws Exception {
        ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
            new Transfer(1L, 20L, BigDecimal.valueOf(200)),
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        assertEquals(StatusResponse.SUCCESS, result.getStatus());
        assertFalse(result.getData());

        List<Transfer> transfers = executeGet(Routes.TRANSFERS_ROUTE, new TypeToken<StandardResponse<List<Transfer>>>() {}.getType());

        for(Transfer transfer : transfers) {
            System.out.println(transfer);
        }

        assertNotNull(transfers);

        assertEquals(1, transfers.size());

        assertFalse(transfers.get(0).isExecuted());
        assertEquals(Long.valueOf(1), transfers.get(0).getFromAccountId());
        assertEquals(Long.valueOf(20), transfers.get(0).getToAccountId());

        AccountAsset accountAsset1 = repositoryManager.getAccountAsset(1L);
        AccountAsset accountAsset20 = repositoryManager.getAccountAsset(20L);

        assertTrue(BigDecimal.valueOf(100).compareTo(accountAsset1.getAmount()) == 0);
        assertNull(accountAsset20);
    }

    @Test
    public void testTransferControllerWithOverSize2() throws Exception {
        ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
            new Transfer(1L, 20L, BigDecimal.valueOf(70)),
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        assertEquals(StatusResponse.SUCCESS, result.getStatus());
        assertTrue(result.getData());

        List<Transfer> transfers = repositoryManager.getTransfers();

        assertNotNull(transfers);

        assertEquals(1, transfers.size());

        assertTrue(transfers.get(0).isExecuted());
        assertEquals(Long.valueOf(1), transfers.get(0).getFromAccountId());
        assertEquals(Long.valueOf(20), transfers.get(0).getToAccountId());

        AccountAsset accountAsset1 = repositoryManager.getAccountAsset(1L);
        AccountAsset accountAsset20 = repositoryManager.getAccountAsset(20L);

        assertTrue(BigDecimal.valueOf(30).compareTo(accountAsset1.getAmount()) == 0);
        assertTrue(BigDecimal.valueOf(70).compareTo(accountAsset20.getAmount()) == 0);

        repositoryManager.deleteAllTransfers();

        result = executePost(Routes.TRANSFER_ROUTE,
            new Transfer(1L, 20L, BigDecimal.valueOf(70)),
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        assertEquals(StatusResponse.SUCCESS, result.getStatus());
        assertFalse(result.getData());

        transfers = repositoryManager.getTransfers();

        assertNotNull(transfers);

        assertEquals(1, transfers.size());

        assertFalse(transfers.get(0).isExecuted());
        assertEquals(Long.valueOf(1), transfers.get(0).getFromAccountId());
        assertEquals(Long.valueOf(20), transfers.get(0).getToAccountId());

        accountAsset1 = repositoryManager.getAccountAsset(1L);
        accountAsset20 = repositoryManager.getAccountAsset(20L);

        assertTrue(BigDecimal.valueOf(30).compareTo(accountAsset1.getAmount()) == 0);
        assertTrue(BigDecimal.valueOf(70).compareTo(accountAsset20.getAmount()) == 0);
    }

    @Test
    public void testTransferControllerWithNegativeAmount() throws Exception {
        ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
            new Transfer(1L, 20L, BigDecimal.valueOf(10).negate()),
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        assertEquals(StatusResponse.ERROR, result.getStatus());
        assertNull(result.getData());

        List<Transfer> transfers = repositoryManager.getTransfers();

        assertNotNull(transfers);

        assertEquals(0, transfers.size());

        AccountAsset accountAsset1 = repositoryManager.getAccountAsset(1L);
        AccountAsset accountAsset20 = repositoryManager.getAccountAsset(20L);

        assertTrue(BigDecimal.valueOf(100).compareTo(accountAsset1.getAmount()) == 0);
        assertNull(accountAsset20);
    }

    @Test
    public void testTransferControllerFromNonExistingAccount() throws Exception {
        ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
            new Transfer(20L, 1L, BigDecimal.valueOf(10)),
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        assertEquals(StatusResponse.ERROR, result.getStatus());
        assertNull(result.getData());

        List<Transfer> transfers = repositoryManager.getTransfers();

        assertNotNull(transfers);

        assertEquals(0, transfers.size());

        AccountAsset accountAsset1 = repositoryManager.getAccountAsset(1L);
        AccountAsset accountAsset20 = repositoryManager.getAccountAsset(20L);

        assertTrue(BigDecimal.valueOf(100).compareTo(accountAsset1.getAmount()) == 0);
        assertNull(accountAsset20);
    }

    @Test
    public void testAssetsController() throws Exception {
        List<AccountAsset> assets = executeGet(Routes.ASSETS_ROUTE, new TypeToken<StandardResponse<List<AccountAsset>>>() {}.getType());

        for(AccountAsset asset : assets) {
            System.out.println(asset);
        }

        assertNotNull(assets);
        assertEquals(4, assets.size());

        for(AccountAsset asset : assets) {
            assertTrue(BigDecimal.valueOf(100).compareTo(asset.getAmount()) == 0);
        }
    }

    @Test
    public void testAssetController() throws Exception {
        AccountAsset[] accountAssets = {
            new AccountAsset(1L, BigDecimal.valueOf(100)),
            new AccountAsset(5L, BigDecimal.valueOf(100)),
            new AccountAsset(6L, BigDecimal.valueOf(100)),
            new AccountAsset(7L, BigDecimal.valueOf(100))
        };

        for(int i = 0; i < accountAssets.length; i++) {
            ResponseData<Boolean> result = executePost(Routes.ASSET_ROUTE,
                accountAssets[i],
                new TypeToken<StandardResponse<Boolean>>() {}.getType()
            );

            assertEquals(StatusResponse.SUCCESS, result.getStatus());
            assertTrue(result.getData());
        }

        AccountAsset asset1 = repositoryManager.getAccountAsset(1L);
        System.out.println(asset1.getAmount());
        assertTrue(BigDecimal.valueOf(200).compareTo(asset1.getAmount()) == 0);

        assertEquals(7, repositoryManager.getAccountAssets().size());
    }

    @Test
    public void testAssetControllerWithNegate() throws Exception {
        ResponseData<Boolean> result = executePost(Routes.ASSET_ROUTE,
            new AccountAsset(1L, BigDecimal.valueOf(70).negate()),
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        assertEquals(StatusResponse.ERROR, result.getStatus());
        assertNull(result.getData());

        AccountAsset asset1 = repositoryManager.getAccountAsset(1L);
        System.out.println(asset1.getAmount());
        assertTrue(BigDecimal.valueOf(100).compareTo(asset1.getAmount()) == 0);

        assertEquals(4, repositoryManager.getAccountAssets().size());
    }

    @Test
    public void testMultiThreadTransfers() throws Exception {
        final int TRANFERS_AMOUNT = 50;
        ExecutorService httpRequestsExecutor = Executors.newFixedThreadPool(10);

        List<Callable<ResponseData<Boolean>>> tasks = new ArrayList<>();

        for(int i = 0; i < TRANFERS_AMOUNT; i ++) {
            tasks.add(() -> {
                Transfer transfer = new Transfer(1L, 10L, BigDecimal.valueOf(1));

                ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
                    transfer,
                    new TypeToken<StandardResponse<Boolean>>() {}.getType()
                );

                assertTrue(result.getData());
                assertEquals(StatusResponse.SUCCESS, result.getStatus());

                return result;
            });
        }

        httpRequestsExecutor.invokeAll(tasks);

        AccountAsset asset1 = repositoryManager.getAccountAsset(1L);
        AccountAsset asset10 = repositoryManager.getAccountAsset(10L);

        System.out.println(asset1);
        System.out.println(asset10);

        List<Transfer> transfers = repositoryManager.getTransfers();

        assertTrue(BigDecimal.valueOf(100 - TRANFERS_AMOUNT).compareTo(asset1.getAmount()) == 0);
        assertTrue(BigDecimal.valueOf(100 - TRANFERS_AMOUNT).compareTo(asset10.getAmount()) == 0);

        assertEquals(50, transfers.size());

        for(Transfer transfer : transfers) {
            assertEquals(Long.valueOf(1), transfer.getFromAccountId());
            assertEquals(Long.valueOf(10), transfer.getToAccountId());
            assertTrue(BigDecimal.valueOf(1).compareTo(transfer.getAmount()) == 0);
        }
    }

    @Test
    public void testMultiThreadTransfers2() throws Exception {
        final int TRANFERS_AMOUNT = 50;
        ExecutorService httpRequestsExecutor = Executors.newFixedThreadPool(4);

        List<Callable<ResponseData<Boolean>>> tasks = new ArrayList<>();

        for(int i = 0; i < TRANFERS_AMOUNT; i ++) {
            tasks.add(() -> {
                Transfer transfer = new Transfer(1L, 10L, BigDecimal.valueOf(1));

                ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
                    transfer,
                    new TypeToken<StandardResponse<Boolean>>() {}.getType()
                );

                assertTrue(result.getData());
                assertEquals(StatusResponse.SUCCESS, result.getStatus());

                return result;
            });

            //Non-executable transfer
            tasks.add(() -> {
                Transfer transfer = new Transfer(1L, 10L, BigDecimal.valueOf(300));

                ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
                    transfer,
                    new TypeToken<StandardResponse<Boolean>>() {}.getType()
                );

                assertFalse(result.getData());
                assertEquals(StatusResponse.SUCCESS, result.getStatus());

                return result;
            });
        }

        httpRequestsExecutor.invokeAll(tasks);

        AccountAsset asset1 = repositoryManager.getAccountAsset(1L);
        AccountAsset asset10 = repositoryManager.getAccountAsset(10L);

        System.out.println(asset1);
        System.out.println(asset10);

        List<Transfer> transfers = repositoryManager.getTransfers();

        assertTrue(BigDecimal.valueOf(100 - TRANFERS_AMOUNT).compareTo(asset1.getAmount()) == 0);
        assertTrue(BigDecimal.valueOf(100 - TRANFERS_AMOUNT).compareTo(asset10.getAmount()) == 0);

        assertEquals(TRANFERS_AMOUNT * 2, transfers.size());

        for(Transfer transfer : transfers) {
            if(transfer.isExecuted()) {
                assertEquals(Long.valueOf(1), transfer.getFromAccountId());
                assertEquals(Long.valueOf(10), transfer.getToAccountId());
                assertTrue(BigDecimal.valueOf(1).compareTo(transfer.getAmount()) == 0);
            }
            else {
                assertEquals(Long.valueOf(1), transfer.getFromAccountId());
                assertEquals(Long.valueOf(10), transfer.getToAccountId());
                assertTrue(BigDecimal.valueOf(300).compareTo(transfer.getAmount()) == 0);
            }
        }
    }

    @Test
    public void testMultiThreadTransfers3() throws Exception {
        final int TRANFERS_AMOUNT = 90;
        ExecutorService httpRequestsExecutor = Executors.newFixedThreadPool(3);

        List<Callable<ResponseData<Boolean>>> tasks = new ArrayList<>();

        for(int i = 0; i < TRANFERS_AMOUNT; i ++) {
            tasks.add(() -> {
                Transfer transfer = new Transfer(1L, 2L, BigDecimal.valueOf(1));

                ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
                    transfer,
                    new TypeToken<StandardResponse<Boolean>>() {}.getType()
                );

                assertTrue(result.getData());
                assertEquals(StatusResponse.SUCCESS, result.getStatus());

                return result;
            });

            //Non-executable transfer
            tasks.add(() -> {
                Transfer transfer = new Transfer(2L, 1L, BigDecimal.valueOf(1));

                ResponseData<Boolean> result = executePost(Routes.TRANSFER_ROUTE,
                    transfer,
                    new TypeToken<StandardResponse<Boolean>>() {}.getType()
                );

                assertTrue(result.getData());
                assertEquals(StatusResponse.SUCCESS, result.getStatus());

                return result;
            });
        }

        httpRequestsExecutor.invokeAll(tasks);

        AccountAsset asset1 = repositoryManager.getAccountAsset(1L);
        AccountAsset asset2 = repositoryManager.getAccountAsset(2L);

        System.out.println(asset1);
        System.out.println(asset2);

        List<Transfer> transfers = repositoryManager.getTransfers();

        assertTrue(BigDecimal.valueOf(100).compareTo(asset1.getAmount()) == 0);
        assertTrue(BigDecimal.valueOf(100).compareTo(asset2.getAmount()) == 0);

        assertEquals(TRANFERS_AMOUNT * 2, transfers.size());

        for(Transfer transfer : transfers) {
            System.out.println(transfer);
            assertTrue(transfer.isExecuted());
            assertEquals(0, BigDecimal.valueOf(1).compareTo(transfer.getAmount()));
        }
    }

    @Test
    public void testValidationForAsset() throws Exception {
        String assetJsonRequest = "{\"accountId\":\"5\",\"amount\":\"-100\"}";

        ResponseData<Boolean> result = executeRawPost(Routes.ASSET_ROUTE,
            assetJsonRequest,
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        assertEquals(StatusResponse.ERROR, result.getStatus());
    }

    @Test
    public void testValidationForTranser1() throws Exception {
        String transferJsonRequest = "{\"fromAccountId\":\"5\",\"amount\":\"100\"}";

        ResponseData<Boolean> result = executeRawPost(Routes.TRANSFER_ROUTE,
            transferJsonRequest,
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        assertEquals(StatusResponse.ERROR, result.getStatus());
    }

    @Test
    public void testValidationForTranser2() throws Exception {
        String transferJsonRequest = "{\"sdfsdfsdfsdfs\":\"5\",\"nzlfteawb\":\"\"}";

        ResponseData<Boolean> result = executeRawPost(Routes.TRANSFER_ROUTE,
            transferJsonRequest,
            new TypeToken<StandardResponse<Boolean>>() {}.getType()
        );

        System.out.println(result);

        assertEquals(StatusResponse.ERROR, result.getStatus());
    }

    private <T> T executeGet(String route, Type type) throws Exception {
        GetMethod get = testServer.get(route, false);

        HttpResponse response = testServer.execute(get);

        assertEquals(200, response.code());

        StandardResponse<T> jsonResponse = JsonUtils.jsonToData(new String(response.body()), type);

        return jsonResponse.getData();
    }

    private <T> ResponseData<T> executePost(String route, Object request, Type type) throws Exception {
        String body = JsonUtils.dataToJson(request);

        PostMethod post = testServer.post(route, body,false);

        HttpResponse response = testServer.execute(post);

        assertEquals(200, response.code());

        StandardResponse<T> jsonResponse = JsonUtils.jsonToData(new String(response.body()), type);

        System.out.println(jsonResponse);

        return new ResponseData<>(jsonResponse.getData(), jsonResponse.getStatus());
    }

    private <T> ResponseData<T> executeRawPost(String route, String request, Type type) throws Exception {
        PostMethod post = testServer.post(route, request,false);

        HttpResponse response = testServer.execute(post);

        assertEquals(200, response.code());

        StandardResponse<T> jsonResponse = JsonUtils.jsonToData(new String(response.body()), type);

        System.out.println(jsonResponse);

        return new ResponseData<>(jsonResponse.getData(), jsonResponse.getStatus());
    }
}
