package org.eeexception.moneytransfer.controllers;

import org.eeexception.moneytransfer.MoneyTransferException;
import org.eeexception.moneytransfer.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Controller to process empty requests.
 */
public abstract class AbstractBaseEmptyRequestController<T> implements Route {
    private static final Logger log = LoggerFactory.getLogger(AbstractBaseEmptyRequestController.class);

    @Override
    public Object handle(Request request, Response response) throws Exception {
        log.info("Received request");

        StandardResponse<T> resp;

        try {
            resp = new StandardResponse<>(StatusResponse.SUCCESS, process());
        }
        catch (MoneyTransferException ex) {
            resp = new StandardResponse<>(StatusResponse.ERROR, ex.getMessage());
        }

        return JsonUtils.dataToJson(resp);
    }

    /**
     *
     * @throws MoneyTransferException - throws if request object is wrong.
     */
    protected abstract T process() throws MoneyTransferException;
}
