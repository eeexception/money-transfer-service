package org.eeexception.moneytransfer.controllers.transfers;

import java.util.List;
import org.eeexception.moneytransfer.MoneyTransferException;
import org.eeexception.moneytransfer.controllers.AbstractBaseEmptyRequestController;
import org.eeexception.moneytransfer.db.RepositoryManager;
import org.eeexception.moneytransfer.entities.Transfer;

/**
 *
 */
public class TransfersController extends AbstractBaseEmptyRequestController<List<Transfer>> {
    private final RepositoryManager repositoryManager;

    /**
     *
     * @param repositoryManager - database repository manager
     */
    public TransfersController(RepositoryManager repositoryManager) {
        this.repositoryManager = repositoryManager;
    }

    @Override
    protected List<Transfer> process() throws MoneyTransferException {
        return repositoryManager.getTransfers();
    }
}
