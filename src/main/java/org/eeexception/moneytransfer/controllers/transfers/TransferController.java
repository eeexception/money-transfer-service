package org.eeexception.moneytransfer.controllers.transfers;

import com.google.gson.reflect.TypeToken;
import java.math.BigDecimal;
import org.eeexception.moneytransfer.MoneyTransferException;
import org.eeexception.moneytransfer.controllers.AbstractBaseController;
import org.eeexception.moneytransfer.db.RepositoryManager;
import org.eeexception.moneytransfer.entities.Transfer;
import org.eeexception.moneytransfer.services.MoneyTransferService;

/**
 *
 */
public class TransferController extends AbstractBaseController<Transfer, Boolean> {
    private final MoneyTransferService moneyTransferService;
    private final RepositoryManager repositoryManager;

    /**
     *
     * @param repositoryManager - database repository manager
     */
    public TransferController(RepositoryManager repositoryManager) {
        super(new TypeToken<Transfer>() {}.getType());

        moneyTransferService = new MoneyTransferService(repositoryManager);
        this.repositoryManager = repositoryManager;
    }

    @Override
    protected Boolean process(Transfer transfer) throws MoneyTransferException {
        return moneyTransferService.executeTransfer(transfer);
    }

    @Override
    protected void validate(Transfer transfer) throws MoneyTransferException {
        if(transfer.getAmount() == null) {
            throw new MoneyTransferException("Amount is null. [transfer=" + transfer + "].");
        }

        if(transfer.getFromAccountId() == null) {
            throw new MoneyTransferException("Account id is null. [transfer=" + transfer + "].");
        }

        if(transfer.getToAccountId() == null) {
            throw new MoneyTransferException("Account id is null. [transfer=" + transfer + "].");
        }

        if(transfer.getFromAccountId() <= 0) {
            throw new MoneyTransferException("Account id less or equals zero. [transfer=" + transfer + "].");
        }

        if(transfer.getToAccountId() <= 0) {
            throw new MoneyTransferException("Account id less or equals zero. [transfer=" + transfer + "].");
        }

        if(transfer.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new MoneyTransferException("Amount less than zero. [transfer=" + transfer + "].");
        }

        if(repositoryManager.getAccountAsset(transfer.getFromAccountId()) == null) {
            throw new MoneyTransferException("Account from does not exist. [transfer=" + transfer + "].");
        }
    }
}
