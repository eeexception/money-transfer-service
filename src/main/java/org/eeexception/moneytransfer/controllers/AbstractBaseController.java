package org.eeexception.moneytransfer.controllers;

import java.lang.reflect.Type;
import org.eeexception.moneytransfer.MoneyTransferException;
import org.eeexception.moneytransfer.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Base controller to process requests.
 */
public abstract class AbstractBaseController<T1, T2> implements Route {
    private static final Logger log = LoggerFactory.getLogger(AbstractBaseController.class);

    private final Type type;

    /**
     * @param type - type for restoring request object from json
     */
    public AbstractBaseController(Type type) {
        this.type = type;
    }

    @Override
    public Object handle(Request request, Response response) throws Exception {
        StandardResponse<T2> resp;

        log.info("Received new request [request={}]", request.body());

        try {
            T1 requestObject = JsonUtils.jsonToData(request.body(), type);

            log.debug("Converted request object from json [object={}]", requestObject);

            validate(requestObject);

            log.debug("Validated request object [object={}]", requestObject);

            resp = new StandardResponse<>(StatusResponse.SUCCESS, process(requestObject));
        }
        catch (MoneyTransferException ex) {
            resp = new StandardResponse<>(StatusResponse.ERROR, ex.getMessage());
        }

        return JsonUtils.dataToJson(resp);
    }

    /**
     *
     * @param request - request object
     * @return processed result object
     * @throws MoneyTransferException - throws if there were error during processing.
     */
    protected abstract T2 process(T1 request) throws MoneyTransferException;

    /**
     *
     * @param request - request object
     * @throws MoneyTransferException - throws if request object is wrong.
     */
    protected abstract void validate(T1 request) throws MoneyTransferException;
}
