package org.eeexception.moneytransfer.controllers;

/**
 * Standard json response model.
 *
 * @param <T>
 */
public class StandardResponse<T> {
    /** Response status: Successful or Error. */
    private StatusResponse status;

    /** Error message */
    private String message;

    /** Response data */
    private T data;

    /**
     * @param status - response status
     * @param message - message
     */
    public StandardResponse(StatusResponse status, String message) {
        this.status = status;
        this.message = message;
    }

    /**
     * @param status - response status
     * @param data - data
     */
    public StandardResponse(StatusResponse status, T data) {
        this.status = status;
        this.data = data;
    }

    public StatusResponse getStatus() {
        return status;
    }

    public void setStatus(StatusResponse status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "StandardResponse{" +
            "status=" + status +
            ", message='" + message + '\'' +
            ", data=" + data +
            '}';
    }
}
