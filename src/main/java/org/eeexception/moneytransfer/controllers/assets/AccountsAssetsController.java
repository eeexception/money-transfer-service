package org.eeexception.moneytransfer.controllers.assets;

import java.util.List;
import org.eeexception.moneytransfer.MoneyTransferException;
import org.eeexception.moneytransfer.controllers.AbstractBaseEmptyRequestController;
import org.eeexception.moneytransfer.db.RepositoryManager;
import org.eeexception.moneytransfer.entities.AccountAsset;

/**
 *
 */
public class AccountsAssetsController extends AbstractBaseEmptyRequestController<List<AccountAsset>> {
    private final RepositoryManager repositoryManager;

    /**
     * @param repositoryManager - database repository manager
     */
    public AccountsAssetsController(RepositoryManager repositoryManager) {
        this.repositoryManager = repositoryManager;
    }

    @Override
    protected List<AccountAsset> process() throws MoneyTransferException {
        return repositoryManager.getAccountAssets();
    }
}
