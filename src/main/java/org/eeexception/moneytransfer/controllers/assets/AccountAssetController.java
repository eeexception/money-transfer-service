package org.eeexception.moneytransfer.controllers.assets;

import com.google.gson.reflect.TypeToken;
import java.math.BigDecimal;
import org.eeexception.moneytransfer.MoneyTransferException;
import org.eeexception.moneytransfer.controllers.AbstractBaseController;
import org.eeexception.moneytransfer.db.RepositoryManager;
import org.eeexception.moneytransfer.entities.AccountAsset;

/**
 *
 */
public class AccountAssetController extends AbstractBaseController<AccountAsset, Boolean> {
    private final RepositoryManager repositoryManager;

    /**
     *
     * @param repositoryManager - database repository manager.
     */
    public AccountAssetController(RepositoryManager repositoryManager) {
        super(new TypeToken<AccountAsset>() {}.getType());

        this.repositoryManager = repositoryManager;
    }

    @Override
    protected Boolean process(AccountAsset accountAsset) throws MoneyTransferException {
        return repositoryManager.updateAsset(accountAsset);
    }

    @Override
    protected void validate(AccountAsset asset) throws MoneyTransferException {
        if(asset.getAmount() == null) {
            throw new MoneyTransferException("Amount is null. [asset=" + asset + "].");
        }

        if(asset.getAccountId() == null) {
            throw new MoneyTransferException("Account id is null. [asset=" + asset + "].");
        }

        if(asset.getAccountId() <= 0) {
            throw new MoneyTransferException("Account id less or equals zero. [asset=" + asset + "].");
        }

        if(asset.getAmount().compareTo(BigDecimal.ZERO) < 0) {
            throw new MoneyTransferException("Amount less than zero. [asset=" + asset + "].");
        }
    }
}
