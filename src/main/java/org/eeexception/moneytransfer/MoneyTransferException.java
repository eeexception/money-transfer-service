package org.eeexception.moneytransfer;

public class MoneyTransferException extends Exception {
    public MoneyTransferException(String message) {
        super(message);
    }
}
