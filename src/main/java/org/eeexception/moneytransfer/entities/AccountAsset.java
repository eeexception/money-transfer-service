package org.eeexception.moneytransfer.entities;

import java.math.BigDecimal;

/**
 * Accounts asset model object.
 */
public class AccountAsset {
    /** Account id */
    private Long accountId;

    /** Asset amount value */
    private BigDecimal amount;

    /**
     * Default contructor.
     *
     * @param accountId - Account id
     * @param amount - Asset amount value
     */
    public AccountAsset(Long accountId, BigDecimal amount) {
        this.accountId = accountId;
        this.amount = amount;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "AccountAsset{" +
            "accountId=" + accountId +
            ", amount=" + amount +
            '}';
    }
}
