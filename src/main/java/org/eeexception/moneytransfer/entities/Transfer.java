package org.eeexception.moneytransfer.entities;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Transfer model object.
 */
public class Transfer {
    /** Account id to send from */
    private Long fromAccountId;

    /** Account id to receive transfer */
    private Long toAccountId;

    /** Amount to transfer */
    private BigDecimal amount;

    /** Transfer execution data and time */
    private Date time;

    /** True if the transfer is executed and false otherwise. */
    private boolean executed;

    /**
     * Default contructor to initialize.
     * The time of the transaction is current time.
     * And the executed value is false by default.
     *
     * @param fromAccountId - Account id to send from
     * @param toAccountId - Account id to receive transfer
     * @param amount - Amount to transfer
     */
    public Transfer(Long fromAccountId, Long toAccountId, BigDecimal amount) {
        this.fromAccountId = fromAccountId;
        this.toAccountId = toAccountId;
        this.amount = amount;
        this.time = new Date(System.currentTimeMillis());
        this.executed = false;
    }

    public Long getFromAccountId() {
        return fromAccountId;
    }

    public void setFromAccountId(Long fromAccountId) {
        this.fromAccountId = fromAccountId;
    }

    public Long getToAccountId() {
        return toAccountId;
    }

    public void setToAccountId(Long toAccountId) {
        this.toAccountId = toAccountId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public boolean isExecuted() {
        return executed;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    @Override
    public String toString() {
        return "Transfer{" +
            "fromAccountId=" + fromAccountId +
            ", toAccountId=" + toAccountId +
            ", amount=" + amount +
            ", time=" + time +
            ", executed=" + executed +
            '}';
    }
}
