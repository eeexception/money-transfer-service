package org.eeexception.moneytransfer;

public class Routes {
    public static final String TRANSFERS_ROUTE = "/transfers";

    public static final String TRANSFER_ROUTE = "/transfers";

    public static final String ASSETS_ROUTE = "/assets";

    public static final String ASSET_ROUTE = "/assets";
}
