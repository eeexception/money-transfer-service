package org.eeexception.moneytransfer.services;

import java.math.BigDecimal;
import org.eeexception.moneytransfer.db.RepositoryManager;
import org.eeexception.moneytransfer.entities.AccountAsset;
import org.eeexception.moneytransfer.entities.Transfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class MoneyTransferService {
    private static final Logger log = LoggerFactory.getLogger(MoneyTransferService.class);
    private final RepositoryManager repositoryManager;

    /**
     * @param repositoryManager - database repository manager
     */
    public MoneyTransferService(RepositoryManager repositoryManager) {
        this.repositoryManager = repositoryManager;
    }

    /**
     * @param transfer - transfer to execute
     *
     * @return - true if executed, false otherwise
     */
    public boolean executeTransfer(Transfer transfer) {
        log.info("Executing new transfer request [transfer={}]", transfer);

        transfer.setExecuted(false);

        if(transfer.getAmount().compareTo(BigDecimal.ZERO) >= 0) {
            if(repositoryManager.updateAsset(new AccountAsset(transfer.getFromAccountId(), transfer.getAmount().negate()))) {
                repositoryManager.updateAsset(new AccountAsset(transfer.getToAccountId(), transfer.getAmount()));

                transfer.setExecuted(true);
            }
        }

        repositoryManager.insertTransfer(transfer);

        return transfer.isExecuted();
    }
}
