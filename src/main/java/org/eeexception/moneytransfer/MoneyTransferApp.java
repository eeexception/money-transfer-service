package org.eeexception.moneytransfer;

import com.google.gson.reflect.TypeToken;
import java.util.List;
import org.eeexception.moneytransfer.controllers.StandardResponse;
import org.eeexception.moneytransfer.controllers.assets.AccountAssetController;
import org.eeexception.moneytransfer.controllers.assets.AccountsAssetsController;
import org.eeexception.moneytransfer.controllers.transfers.TransferController;
import org.eeexception.moneytransfer.controllers.transfers.TransfersController;
import org.eeexception.moneytransfer.db.DefaultRepositoryManager;
import org.eeexception.moneytransfer.db.RepositoryManager;
import org.eeexception.moneytransfer.entities.AccountAsset;
import org.eeexception.moneytransfer.entities.Transfer;
import org.eeexception.moneytransfer.utils.JsonUtils;

import static spark.Spark.get;
import static spark.Spark.port;
import static spark.Spark.post;

/**
 * Main application class.
 * Defines and initiates instance of the repository manager, initiates http routes and starts the web server.
 */
public class MoneyTransferApp {
    /** Default http port for the web server */
    private static final int DEFAULT_HTTP_PORT = 8080;

    /**
     * Main java application class.
     *
     * @param args - application arguments.
     */
    public static void main(String[] args) throws MoneyTransferException {
        RepositoryManager repositoryManager = new DefaultRepositoryManager();

        if(args != null) {
            if(args.length != 0) {
                if (args.length != 1) {
                    throw new MoneyTransferException("Invalid amount of the application arguments.");
                }

                initAccounts(repositoryManager, args[0]);
            }
        }

        port(DEFAULT_HTTP_PORT);

        initRoutes(repositoryManager);
    }

    /**
     * Initialize data at the database.
     */
    public static void initAccounts(RepositoryManager repositoryManager, String jsonString) throws MoneyTransferException {
        List<AccountAsset> assets = JsonUtils.jsonToData(jsonString, new TypeToken<List<AccountAsset>>() {}.getType());

        for(AccountAsset asset : assets) {
            repositoryManager.updateAsset(asset);
        }
    }

    /**
     * Http routes initialization and definition controllers for these routes.
     *
     * @param repositoryManager - database repository manager, which is required to initialize controllers.
     */
    public static void initRoutes(RepositoryManager repositoryManager) {
        get(Routes.TRANSFERS_ROUTE, new TransfersController(repositoryManager));
        post(Routes.TRANSFER_ROUTE, "application/json", new TransferController(repositoryManager));
        get(Routes.ASSETS_ROUTE, new AccountsAssetsController(repositoryManager));
        post(Routes.ASSET_ROUTE,"application/json", new AccountAssetController(repositoryManager));
    }
}
