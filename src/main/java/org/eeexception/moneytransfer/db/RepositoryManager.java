package org.eeexception.moneytransfer.db;

import java.util.List;
import org.eeexception.moneytransfer.entities.AccountAsset;
import org.eeexception.moneytransfer.entities.Transfer;

/**
 * Repository manager.
 * Provides methods to store and read data from the database.
 */
public interface RepositoryManager {
    /**
     * Insert {@link Transfer} object to the database.
     *
     * @param transfer - object to insert.
     */
    void insertTransfer(Transfer transfer);

    /**
     * Inserts or updates account asset record at the database.
     *
     * @param asset - object to insert.
     * @return - returns false if there were no insert or update and the record was not changed.
     */
    boolean updateAsset(AccountAsset asset);

    /**
     * Receives account assets from the database.
     *
     * @param accountId - account id to receive it's asset.
     * @return {@link AccountAsset} object
     */
    AccountAsset getAccountAsset(long accountId);

    /**
     * Receives all account assets records from the database.
     *
     * @return List of the {@link AccountAsset} objects.
     */
    List<AccountAsset> getAccountAssets();

    /**
     * Receives all transfers records from the database.
     *
     * @return List of the {@link Transfer} objects.
     */
    List<Transfer> getTransfers();

    /**
     * Removes all asset records from the database.
     */
    void deleteAllAssets();

    /**
     * Removes all transfer records from the database.
     */
    void deleteAllTransfers();
}
