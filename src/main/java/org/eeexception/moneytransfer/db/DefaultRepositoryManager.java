package org.eeexception.moneytransfer.db;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.eeexception.moneytransfer.entities.AccountAsset;
import org.eeexception.moneytransfer.entities.Transfer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Default implementation for repository manager using in memory storage.
 */
public class DefaultRepositoryManager implements RepositoryManager {
    private static final Logger log = LoggerFactory.getLogger(DefaultRepositoryManager.class);

    private class AssetTrasfer {
        private AccountAsset accountAsset;
        private BigDecimal lastTransferAmount;

        public AssetTrasfer(AccountAsset accountAsset, BigDecimal lastTransferAmount) {
            this.accountAsset = accountAsset;
            this.lastTransferAmount = lastTransferAmount;
        }

        public AccountAsset getAccountAsset() {
            return accountAsset;
        }

        public BigDecimal getLastTransferAmount() {
            return lastTransferAmount;
        }

        @Override
        public String toString() {
            return "AssetTrasfer{" +
                "accountAsset=" + accountAsset +
                ", lastTransferAmount=" + lastTransferAmount +
                '}';
        }
    }

    private final ConcurrentHashMap<Long, AssetTrasfer> assets;
    private final List<Transfer> transfers;

    /**
     *
     */
    public DefaultRepositoryManager() {
        assets = new ConcurrentHashMap<>();
        transfers = Collections.synchronizedList(new ArrayList<>());
    }

    @Override
    public void insertTransfer(Transfer transfer) {
        transfers.add(transfer);
    }
//
//    @Override
//    public boolean updateAssets(final AccountAsset[] updatedAssets) {
//        Map<Long, AccountAsset> modifiedAssets = new HashMap<>(assets);
//
//        for(AccountAsset asset : updatedAssets) {
//            if (!updateAssets(asset)) {
//                return false;
//            }
//        }
//
//        assets.putAll(modifiedAssets);
//
//        return true;
//    }
//
//    @Override
//    public boolean updateAsset(final AccountAsset asset) {
//        return updateAssets(asset);
//    }

    @Override
    public AccountAsset getAccountAsset(long accountId) {
        AssetTrasfer assetTrasfer = assets.get(accountId);

        if(assetTrasfer == null) {
            return null;
        }

        return assetTrasfer.getAccountAsset();
    }

    @Override
    public List<AccountAsset> getAccountAssets() {
        return assets.values().stream().map(AssetTrasfer::getAccountAsset).collect(Collectors.toList());
    }

    @Override
    public List<Transfer> getTransfers() {
        return new ArrayList<>(transfers);
    }

    @Override
    public void deleteAllAssets() {
        assets.clear();
    }

    @Override
    public void deleteAllTransfers() {
        transfers.clear();
    }

    @Override
    public boolean updateAsset(final AccountAsset asset) {
        AssetTrasfer modifiedAsset = assets.compute(asset.getAccountId(), (key, value) -> computeAccountAsset(asset, key, value));

        if(modifiedAsset.getLastTransferAmount() == null) {
            return false;
        }

        return true;
    }

    private AssetTrasfer computeAccountAsset(AccountAsset newAsset, Long key, AssetTrasfer value) {
        if (value == null) {
            log.debug("Value is null for key={}, assign new value={}", key, newAsset.getAmount());

            if (newAsset.getAmount().compareTo(BigDecimal.ZERO) >= 0) {
                return new AssetTrasfer(newAsset, newAsset.getAmount());
            }
            else {
                return null;
            }
        }
        else {
            BigDecimal computedValue = newAsset.getAmount().add(value.getAccountAsset().getAmount());

            log.debug("Value is existing for key={}, [old-value={}, new-value={}]", key, value, computedValue);

            if (computedValue.compareTo(BigDecimal.ZERO) >= 0) {
                return new AssetTrasfer(new AccountAsset(key, computedValue), newAsset.getAmount());
            }
            else {
                return new AssetTrasfer(value.getAccountAsset(), null);
            }
        }
    }
}
