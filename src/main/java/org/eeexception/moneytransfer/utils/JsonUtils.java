package org.eeexception.moneytransfer.utils;

import com.google.gson.Gson;
import java.lang.reflect.Type;

/**
 * Utility class for converting from/to json.
 * All usage of the json is inside this utility class.
 */
public class JsonUtils {
    /** Data convertation instance. */
    private static final Gson gson = new Gson();

    private JsonUtils() {
    }

    /**
     * Converts object to string json.
     *
     * @param data - object to convert.
     * @return - json string
     */
    public static String dataToJson(Object data) {
        return gson.toJson(data);
    }

    /**
     * Converts json string to object.
     *
     * @param json - json string
     * @param type - object type.
     * @param <T> - object type
     * @return Object
     */
    public static <T> T jsonToData(String json, Type type) {
        return gson.fromJson(json, type);
    }
}
